import datetime

import pydicom
from pydicom.dataset import Dataset
from pynetdicom3 import (
    AE,
    StoragePresentationContexts,
    PYNETDICOM_IMPLEMENTATION_UID,
    PYNETDICOM_IMPLEMENTATION_VERSION
)
from pynetdicom3.sop_class import PatientRootQueryRetrieveInformationModelFind

def listen(aet, port, callback):
    ae = AE(aet, port)
    ae.supported_contexts = StoragePresentationContexts
    ae.requested_contexts = StoragePresentationContexts

    def on_c_store(ds, context, info):
        """Store the pydicom Dataset `ds`.

        Parameters
        ----------
        ds : pydicom.dataset.Dataset
            The dataset that the peer has requested be stored.
        context : namedtuple
            The presentation context that the dataset was sent under.
        info : dict
            Information about the association and storage request.

        Returns
        -------
        status : int or pydicom.dataset.Dataset
            The status returned to the peer AE in the C-STORE response. Must be
            a valid C-STORE status value for the applicable Service Class as
            either an ``int`` or a ``Dataset`` object containing (at a
            minimum) a (0000,0900) *Status* element.
        """
        # Add the DICOM File Meta Information
        meta = Dataset()
        meta.MediaStorageSOPClassUID = ds.SOPClassUID
        meta.MediaStorageSOPInstanceUID = ds.SOPInstanceUID
        meta.ImplementationClassUID = PYNETDICOM_IMPLEMENTATION_UID
        meta.ImplementationVersionName = PYNETDICOM_IMPLEMENTATION_VERSION
        meta.TransferSyntaxUID = context.transfer_syntax

        # Add the file meta to the dataset
        ds.file_meta = meta

        # Set the transfer syntax attributes of the dataset
        ds.is_little_endian = context.transfer_syntax.is_little_endian
        ds.is_implicit_VR = context.transfer_syntax.is_implicit_VR

        callback(ds)

        # Return a 'Success' status
        return 0x0000

    ae.on_c_store = on_c_store
    ae.start()

def send(aet, port, ds):
    ae = AE()
    ae.supported_contexts = StoragePresentationContexts
    ae.requested_contexts = StoragePresentationContexts

    assoc = ae.associate(aet, port)
    if assoc.is_established:
        # Use the C-STORE service to send the dataset
        # returns a pydicom Dataset
        status = assoc.send_c_store(ds)

        # Check the status of the storage request
        if 'Status' in status:
            # If the storage request succeeded this will be 0x0000
            print('C-STORE request status: 0x{0:04x}'.format(status.Status))
        else:
            print('Connection timed out or invalid response from peer')

        # Release the association
        assoc.release()
    else:
        print('Association rejected or aborted')
