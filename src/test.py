from pydicom import dcmread
from pydicom.uid import generate_uid
from radiobotics import listen, send

pacs_host = '127.0.0.1'
pacs_port = 4242

# on store callback
def on_store(dataset):
    filepath = 'storage/' + dataset.SOPInstanceUID

    # OPTION 1:
    # save to disk
    dataset.save_as(filepath, write_like_original=False)
    # YOUR PIPELINE HERE
    pass
    # load from disk
    dataset = dcmread(filepath)

    # OPTION 2:
    # modify dataset directly
    dataset.PatientName = '{} (MODIFIED)'.format(dataset.PatientName)
    dataset.PatientID = generate_uid()

    # send modifed dataset
    send(pacs_host, pacs_port, dataset)

# start listening
aet = 'RADIOBOTICS'
port = 4243
listen(aet, port, on_store)
