## Example for Radiobotics

We are using python libraries

* `pydicom` for DICOM file handling
* `pynetdicom` for PACS communication

Install them using

```
pipenv install
```

Run orthanc (PACS-like server) using

```
docker-compose up -d
```

Then run example script

```
pipenv run python src/test.py
```

To send files:
* open `http://localhost:8042/`
* click upload
* drag and drop DICOM files
* press "Start the upload"
* go back (press "Patients")
* click on a patient
* click "Send to remote modality"
* click on "test" modality.
